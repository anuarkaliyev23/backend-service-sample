package org.bitbucket.anuarkaliyev23.film.sample.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Objects;

/**
 * Класс, хранящий информацию о работе.
 * Один и тот же человек может выполнять разные работы в фильме (пример - Продюсер/Сценарист/Режиссер одновременно)
 * С помощью аннотаций ORMLite мы объясняем, что это на самом деле структура таблицы, под названием "job"
 * */
@DatabaseTable(tableName = "job")
public class Job {
    /**
     * Уникальный ID, первичный ключ.
     * generatedId = true - Мы говорим, что хотим autoincrement
     * columnName = "id" - Мы говорим, что соответствующая колонка называется id.
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге
     * */
    @DatabaseField(generatedId = true, columnName = "id")
    private long id;

    /**
     * Название работы (Продюсер/Актер/Костюмер/Сценарист и т.д.)
     * canBeNull = false - В БАЗЕ ДАННЫХ поле не может быть NULL (NOT NULL constraint - в SQL)
     * columnName - Мы указываем название колонки в БД
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге
     * */
    @DatabaseField(canBeNull = false, columnName = "title")
    private String title;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Job job = (Job) o;
        return id == job.id &&
                Objects.equals(title, job.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title);
    }

    @Override
    public String toString() {
        return "Job{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Job() {
    }

    public Job(long id, String title) {
        this.id = id;
        this.title = title;
    }
}
