package org.bitbucket.anuarkaliyev23.film.sample.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Класс, хранящий информацию о человеке.
 * Один и тот же человек может выполнять разные работы в фильме (пример - Продюсер/Сценарист/Режиссер одновременно)
 * С помощью аннотаций ORMLite мы объясняем, что это на самом деле структура таблицы, под названием "person"
 * */
@DatabaseTable(tableName = "person")
public class Person {
    /**
     * Уникальный ID, первичный ключ.
     * generatedId = true - Мы говорим, что хотим autoincrement
     * columnName = "id" - Мы говорим, что соответствующая колонка называется id.
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге
     * */
    @DatabaseField(generatedId = true, columnName = "id")
    private long id;

    /**
     * Имя
     * canBeNull = false - В БАЗЕ ДАННЫХ поле не может быть NULL (NOT NULL constraint - в SQL)
     * columnName - Мы указываем название колонки в БД
     *
     * Здесь названия Колонки в БД и свойста объекта разные. Указывать название колонки обязательно, если не хотите,
     * чтобы ORMLite назвал колонку называнием свойства в таблице.
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге.
     * */
    @DatabaseField(canBeNull = false, columnName = "first_name")
    private String firstName;


    /**
     * Фамилия
     * canBeNull = false - В БАЗЕ ДАННЫХ поле не может быть NULL (NOT NULL constraint - в SQL)
     * columnName - Мы указываем название колонки в БД
     *
     * Здесь названия Колонки в БД и свойста объекта разные. Указывать название колонки обязательно, если не хотите,
     * чтобы ORMLite назвал колонку называнием свойства в таблице.
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге.
     * */
    @DatabaseField(canBeNull = false, columnName = "last_name")
    private String lastName;

    /**
     * Дата рождения
     * canBeNull = false - В БАЗЕ ДАННЫХ поле не может быть NULL (NOT NULL constraint - в SQL)
     * columnName - Мы указываем название колонки в БД
     * dataType = DataType.SERIALIZABLE - SQLite не знает как хранить Java 8 дату. Поэтому мы говорим ему не париться и просто хранить по байтам.
     *
     * Здесь названия Колонки в БД и свойста объекта разные. Указывать название колонки обязательно, если не хотите,
     * чтобы ORMLite назвал колонку называнием свойства в таблице.
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге.
     * */
    @DatabaseField(canBeNull = false, columnName = "birth_date", dataType = DataType.SERIALIZABLE)
    private LocalDate birthday;

    /**
     * Для удобства дата рождения назначается автоматически при создании объекта.
     * Ее, тем не менее, можно поменять.
     * */
    public Person() {
        birthday = LocalDate.now();
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(birthday, person.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, birthday);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }
}
