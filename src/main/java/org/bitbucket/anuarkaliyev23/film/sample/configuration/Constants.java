package org.bitbucket.anuarkaliyev23.film.sample.configuration;

/**
* Класс констант содержит в себе (внезапно) разные константы для проекта
*
* В идеале такие вещи нужно выносить не просто в отдельный класс, а в отдельный файл настроек,
* чтобы можно было менять их в будущем без залезания в код.
* */
public class Constants {
    /**Порт, на котором запускается джавалин*/
    public static final int PORT = 7645;

    /**Путь до базы (относительный).Подразуменвается, что файл лежит в папке проекта*/
    public static final String DB_PATH = "jdbc:sqlite:film.db";

    //HTTP Response Status codes
    /**Отдается, когда все хорошо*/
    public static final int OK_200 = 200;
    /**Отдается, когда мы успешно создали новую запись в БД*/
    public static final int CREATED_201 = 201;
    /**Отдается, когда не найден путь или не найдена запись в базе*/
    public static final int NOT_FOUND_404 = 404;
    /**Отдается, когда на сервере произошла какая-либо ошибка*/
    public static final int INTERNAL_SERVER_ERROR_500 = 500;
    /**Отдается, когда клиент неправильно оформил запрос*/
    public static final int BAD_REQUEST_400 = 400;

    /**Приватный конструктор - чтобы никто не смог инициализировать объект этого класса (по ошибке)*/
    private Constants() {}
}
