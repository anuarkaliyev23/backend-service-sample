package org.bitbucket.anuarkaliyev23.film.sample.exceptions;

/**
 * Будущий предок всех наших исключений в проекте.
 * Иметь общего предка удобно, если вы хотите поймать все
 * исключения связанные с проектом - то нужно писать всего один catch
 * */
public class GeneralAppException extends RuntimeException {
    public GeneralAppException() {
    }

    public GeneralAppException(String message) {
        super(message);
    }

    public GeneralAppException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeneralAppException(Throwable cause) {
        super(cause);
    }

    public GeneralAppException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
