package org.bitbucket.anuarkaliyev23.film.sample.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Objects;

/**
 * Модель, хранящая информацию о жанре фильма
 * С помощью аннотаций ORMLite мы объясняем, что это на самом деле структура таблицы, под названием "genres"
 * */
@DatabaseTable(tableName = "genre")
public class Genre {

    /**
     * Уникальный ID, первичный ключ.
     * generatedId = true - Мы говорим, что хотим autoincrement
     * columnName = "id" - Мы говорим, что соответствующая колонка называется id.
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге
     * */
    @DatabaseField(generatedId = true, columnName = "id")
    private long id;

    /**
     * Название жанра
     * canBeNull = false - В БАЗЕ ДАННЫХ поле не может быть NULL (NOT NULL constraint - в SQL)
     * columnName - Мы указываем название колонки в БД
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге
     * */
    @DatabaseField(canBeNull = false,columnName = "name")
    private String name;

    public Genre() {
    }

    public Genre(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return id == genre.id &&
                Objects.equals(name, genre.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
