package org.bitbucket.anuarkaliyev23.film.sample;

import io.javalin.Javalin;
import io.javalin.json.JavalinJackson;
import org.bitbucket.anuarkaliyev23.film.sample.configuration.Constants;
import org.bitbucket.anuarkaliyev23.film.sample.configuration.JacksonUtils;
import org.bitbucket.anuarkaliyev23.film.sample.controller.*;

import static io.javalin.apibuilder.ApiBuilder.crud;

public class Main {
    public static void main(String[] args) {
        //Запускаем Javalin С логированием запросов на порту указанном в константах
        Javalin app = Javalin.create()
                             .enableDebugLogging()
                             .port(Constants.PORT);
        //Говорим Javalin какой ObjectMapper использовать для сериализации\десериализации с помощью Jackson.
        JavalinJackson.configure(JacksonUtils.getMapper());

        //Создаем пути
        /*
         * Создаются следующие пути:
         * GET films/
         * GET films/:id
         * POST films/
         * PATCH films/:id
         * DELETE films/:id
         *
         * GET genres/
         * GET genres/:id
         * POST genres/
         * PATCH genres/:id
         * DELETE genres/:id
         *
         * GET jobs/
         * GET jobs/:id
         * POST jobs/
         * PATCH jobs/:id
         * DELETE jobs/:id
         *
         * GET persons/
         * GET persons/:id
         * POST persons/
         * PATCH persons/:id
         * DELETE persons/:id
         *
         *
         * GET fg/
         * GET fg/:id
         * POST fg/
         * PATCH fg/:id
         * DELETE fg/:id
         *
         * GET fp/
         * GET fp/:id
         * POST fp/
         * PATCH fp/:id
         * DELETE fp/:id
        * */
        app.routes(() -> {
            crud("films/:id", new FilmController());
            crud("genres/:id", new GenreController());
            crud("jobs/:id", new JobController());
            crud("persons/:id", new PersonController());
            crud("fg/:id", new FilmGenreController());
            crud("fp/:id", new FilmPersonController());
        });

        //Стартуем
        app.start();
    }
}
