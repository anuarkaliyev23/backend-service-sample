package org.bitbucket.anuarkaliyev23.film.sample.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * Утилиты для сериализации/десериализации с помощью Jackson
 * */
public class JacksonUtils {
    /**
     * Создаем маппер и регистрируем модуль JavaTimeModule, который умеет работать с Java 8 классами времени и даты (LocalDate, LocalDateTime etc.)
     *
     * @see java.time.LocalDate
     * @see java.time.LocalDateTime
     * @see java.time.ZonedDateTime
     * */
    private static ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());

    /**
     * Приватный конструктор, чтобы никто не смог создать объект класса (по ошибке)
     * */
    private JacksonUtils(){}

    /**
     * @return Маппер с нужными нам в проекте модулями
     * */
    public static ObjectMapper getMapper() {
        return mapper;
    }
}
