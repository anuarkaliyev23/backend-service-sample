package org.bitbucket.anuarkaliyev23.film.sample.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Objects;

/**
 * Таблица, связывающая Фильмы, Людей и их Работы
 *
 * @see Film
 * @see Person
 * @see Job
 * */
@DatabaseTable(tableName = "film_person")
public class FilmPerson {
    /**
     * Уникальный ID, первичный ключ.
     * generatedId = true - Мы говорим, что хотим autoincrement
     * columnName = "id" - Мы говорим, что соответствующая колонка называется id.
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге
     * */
    @DatabaseField(generatedId = true, columnName = "id")
    private long id;


    /**
     * FOREIGN KEY на таблицу людей.
     * Как видите, вместо того, чтобы хранить long, мы храним самого человека. Поэтому мы объясняем, что в БД
     * на самом деле это FK с помощью аннотаций.
     *
     * foreign = true - Мы говорим, что это ссылка
     * foreignAutoRefresh = true - Мы говорим, что хотим, чтобы ORMLite сам следил за сущностями
     *      и обновлял данные соответствующе (например при изменении записей в ссылочном фильме В БД, чтобы он сам перенастроил ссылки на объект и показывал нам актуальную информацию)
     * foreignAutoCreate = true - Мы говорим, что если он не нашел по ссылке запись, пусть создаст ее сам.
     * */
    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true, columnName = "person_id")
    private Person person;


    /**
     * FOREIGN KEY на таблицу фильмов.
     * Как видите, вместо того, чтобы хранить long, мы храним сам фильм. Поэтому мы объясняем, что в БД
     * на самом деле это FK с помощью аннотаций.
     *
     * foreign = true - Мы говорим, что это ссылка
     * foreignAutoRefresh = true - Мы говорим, что хотим, чтобы ORMLite сам следил за сущностями
     *      и обновлял данные соответствующе (например при изменении записей в ссылочном фильме В БД, чтобы он сам перенастроил ссылки на объект и показывал нам актуальную информацию)
     * foreignAutoCreate = true - Мы говорим, что если он не нашел по ссылке запись, пусть создаст ее сам.
     * */
    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true, columnName = "film_id")
    private Film film;


    /**
     * FOREIGN KEY на таблицу работ.
     * Как видите, вместо того, чтобы хранить long, мы храним саму работу. Поэтому мы объясняем, что в БД
     * на самом деле это FK с помощью аннотаций.
     *
     * foreign = true - Мы говорим, что это ссылка
     * foreignAutoRefresh = true - Мы говорим, что хотим, чтобы ORMLite сам следил за сущностями
     *      и обновлял данные соответствующе (например при изменении записей в ссылочном фильме В БД, чтобы он сам перенастроил ссылки на объект и показывал нам актуальную информацию)
     * foreignAutoCreate = true - Мы говорим, что если он не нашел по ссылке запись, пусть создаст ее сам.
     * */
    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true, columnName = "person_job")
    private Job job;

    @Override
    public String toString() {
        return "FilmPerson{" +
                "id=" + id +
                ", person=" + person +
                ", film=" + film +
                ", job=" + job +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilmPerson that = (FilmPerson) o;
        return id == that.id &&
                Objects.equals(person, that.person) &&
                Objects.equals(film, that.film) &&
                Objects.equals(job, that.job);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, person, film, job);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public FilmPerson(long id, Person person, Film film, Job job) {
        this.id = id;
        this.person = person;
        this.film = film;
        this.job = job;
    }

    public FilmPerson() {
    }
}
