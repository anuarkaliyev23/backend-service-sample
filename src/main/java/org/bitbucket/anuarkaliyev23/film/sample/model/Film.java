package org.bitbucket.anuarkaliyev23.film.sample.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Objects;

/**
 * Класс, хранящий информацию о фильме.
 * С помощью аннотаций ORMLite мы объясняем, что это на самом деле структура таблицы, под названием "films"
 * */
@DatabaseTable(tableName = "films")
public class Film {

    /**
     * Уникальный ID, первичный ключ.
     * generatedId = true - Мы говорим, что хотим autoincrement
     * columnName = "id" - Мы говорим, что соответствующая колонка называется id.
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге
     * */
    @DatabaseField(generatedId = true, columnName = "id")
    private long id;

    /**
     * Название фильма
     * canBeNull = false - В БАЗЕ ДАННЫХ поле не может быть NULL (NOT NULL constraint - в SQL)
     * columnName - Мы указываем название колонки в БД
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге
     * */
    @DatabaseField(canBeNull = false, columnName = "title")
    private String title;

    /**
     * Год выпуска фильма
     * canBeNull = false - В БАЗЕ ДАННЫХ поле не может быть NULL (NOT NULL constraint - в SQL)
     * columnName - Мы указываем название колонки в БД
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге
     * */
    @DatabaseField(canBeNull = false, columnName = "year")
    private int year;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Film film = (Film) o;
        return id == film.id &&
                year == film.year &&
                Objects.equals(title, film.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, year);
    }

    @Override
    public String toString() {
        return "Film{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", year=" + year +
                '}';
    }

    public Film() { }

    public Film(long id, String title, int year) {
        this.id = id;
        this.title = title;
        this.year = year;
    }
}
