package org.bitbucket.anuarkaliyev23.film.sample.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Objects;


/**
 * Таблица, связывающая Жанры и Фильмы
 * @see Film
 * @see Genre
 * */
@DatabaseTable(tableName = "film_genre")
public class FilmGenre {

    /**
     * Уникальный ID, первичный ключ.
     * generatedId = true - Мы говорим, что хотим autoincrement
     * columnName = "id" - Мы говорим, что соответствующая колонка называется id.
     *
     * Если название поля и колонки совпадают, то указывать имя не обязательно, но остается на ваше усмотрение.
     * Я решил указывать всегда для наглядности и легкости в рефакторинге
     * */
    @DatabaseField(generatedId = true, columnName = "id")
    private long id;

    /**
     * FOREIGN KEY на таблицу фильмов.
     * Как видите, вместо того, чтобы хранить long, мы храним сам фильм. Поэтому мы объясняем, что в БД
     * на самом деле это FK с помощью аннотаций.
     *
     * foreign = true - Мы говорим, что это ссылка
     * foreignAutoRefresh = true - Мы говорим, что хотим, чтобы ORMLite сам следил за сущностями
     *      и обновлял данные соответствующе (например при изменении записей в ссылочном фильме В БД, чтобы он сам перенастроил ссылки на объект и показывал нам актуальную информацию)
     * foreignAutoCreate = true - Мы говорим, что если он не нашел по ссылке запись, пусть создаст ее сам.
     * */
    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true, columnName = "film_id")
    private Film film;

    /**
     * FOREIGN KEY на таблицу жанров.
     * Как видите, вместо того, чтобы хранить long, мы храним сам жанр. Поэтому мы объясняем, что в БД
     * на самом деле это FK с помощью аннотаций.
     *
     * foreign = true - Мы говорим, что это ссылка
     * foreignAutoRefresh = true - Мы говорим, что хотим, чтобы ORMLite сам следил за сущностями
     *      и обновлял данные соответствующе (например при изменении записей в ссылочном фильме В БД, чтобы он сам перенастроил ссылки на объект и показывал нам актуальную информацию)
     * foreignAutoCreate = true - Мы говорим, что если он не нашел по ссылке запись, пусть создаст ее сам.
     * */
    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true, columnName = "genre_id")
    private Genre genre;

    @Override
    public String toString() {
        return "FilmGenre{" +
                "id=" + id +
                ", film=" + film +
                ", genre=" + genre +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilmGenre filmGenre = (FilmGenre) o;
        return id == filmGenre.id &&
                Objects.equals(film, filmGenre.film) &&
                Objects.equals(genre, filmGenre.genre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, film, genre);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public FilmGenre(long id, Film film, Genre genre) {
        this.id = id;
        this.film = film;
        this.genre = genre;
    }

    public FilmGenre() {
    }
}
