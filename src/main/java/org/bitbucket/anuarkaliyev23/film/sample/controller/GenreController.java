package org.bitbucket.anuarkaliyev23.film.sample.controller;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import io.javalin.Context;
import io.javalin.apibuilder.CrudHandler;
import org.bitbucket.anuarkaliyev23.film.sample.configuration.Constants;
import org.bitbucket.anuarkaliyev23.film.sample.configuration.DatabaseUtils;
import org.bitbucket.anuarkaliyev23.film.sample.model.Film;
import org.bitbucket.anuarkaliyev23.film.sample.model.FilmPerson;
import org.bitbucket.anuarkaliyev23.film.sample.model.Genre;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

/**
 * Контроллер для модели Genre
 *
 * {@link CrudHandler} - Интерфейс джавалина, с помощью которого мы можем описать классическое CRUD-поведение.
 * CRUD расшифровывается как CREATE, READ, UPDATE, DELETE.
 * То есть CRUD - методы, служащие для создания, чтения, изменения и удаления записей из БД.
 * Мы, конечно, можем им не пользоваться, и делать все вручную (но я не стал)
 *
 * Существует конвенция соответствия HTTP реквестов CRUD методам
 * HTTP POST - CREATE
 * HTTP GET - READ
 * HTTP PATCH - UPDATE
 * HTTP DELETE - DELETE
 *
 * То есть создаются пути
 * (САМИ ПУТИ ОПИСЫВАЮТСЯ В {@link org.bitbucket.anuarkaliyev23.film.sample.Main}! ЗДЕСЬ ПРОСТО COPY-PASTE)
 * GET genres/
 * GET genres/:id
 * POST genres/
 * PATCH genres/:id
 * DELETE genres/:id
 *
 * @see org.bitbucket.anuarkaliyev23.film.sample.Main - там лежат пути javalin
 * Пожалкйста, обратитесь к документации javalin, если хотите узнать дополнительно про CrudHandler.
 *
 * @see Genre
 * */
public class GenreController implements CrudHandler {
    /**
     * DAO - паттерн разработки, при котором вся логика доступа к БД прячется в Data Access Object - DAO.
     * То есть любые операции, требующие изменения или считывания с БД должны идти через объект-посредник DAO.
     *
     * У ORMLite есть своя реализация этого паттерна, выраженная в классе Dao.
     * Dao<Genre, Long> - Мы указываем, что это ДАО к сущности Фильм, а вторым параметром мы указываем ПЕРВИЧНЫЙ КЛЮЧ. То есть Long - в нашем случае
     * (Потому, что {@link Genre#id - long})
     *
     * @see Dao
     * */
    private Dao<Genre, Long> dao;

    /**
     * Логгер, чтобы мы могли писать в консольку и лучше понимать, что происходит, и если что-то пойдет не так,
     * понять это быстрее.
     * */
    private Logger logger;

    public GenreController() {
        logger = LoggerFactory.getLogger(this.getClass());
        try {
            dao = DaoManager.createDao(DatabaseUtils.getSource(), Genre.class);
        } catch (SQLException e) {
            logger.error("Error creating DAO");
        }
    }

    /**
     * Метод, отвечающий за создание новой записи в БД.
     * Соответствует POST методу в HTTP. (POST genres/)
     *
     * "@NotNull" - контракт в Java, который говорит, что если Context == null, выбросить исключение и остановить программу.
     * */
    @Override
    public void create(@NotNull Context context) {
        Genre genre = context.bodyAsClass(Genre.class);
        try {
            dao.create(genre);
            context.status(Constants.CREATED_201);
        } catch (SQLException e) {
            logger.error("Error occurred saving record");
            context.status(Constants.INTERNAL_SERVER_ERROR_500);
        }
    }


    /**
     * Метод, отвечающий за удаление записи в БД.
     * Соответствует DELETE методу в HTTP. (DELETE genres/:id)
     *
     * "@NotNull" - контракт в Java, который говорит, что если Context == null, выбросить исключение и остановить программу.
     * */
    @Override
    public void delete(@NotNull Context context, @NotNull String s) {
        long genreId = Long.valueOf(s);
        try {
            dao.deleteById(genreId);
        } catch (SQLException e) {
            logger.error("Error occurred deleting records");
            context.status(Constants.INTERNAL_SERVER_ERROR_500);
        }
    }

    /**
     * Метод, отвечающий за считывание ВСЕХ записей с БД.
     * Соответствует GET методу в HTTP. (GET genres/)
     *
     * "@NotNull" - контракт в Java, который говорит, что если Context == null, выбросить исключение и остановить программу.
     * */
    @Override
    public void getAll(@NotNull Context context) {
        try {
            context.json(dao.queryForAll());
        } catch (SQLException e) {
            logger.error("Error occurred listing records");
            context.status(Constants.INTERNAL_SERVER_ERROR_500);
        }
    }

    /**
     * Метод, отвечающий за считывание ОДНОЙ записи с БД.
     * Соответствует GET методу в HTTP. (GET genres/:id)
     *
     * "@NotNull" - контракт в Java, который говорит, что если Context == null, выбросить исключение и остановить программу.
     * */
    @Override
    public void getOne(@NotNull Context context, @NotNull String s) {
        long genreId = Long.valueOf(s);
        try {
            Genre genre = dao.queryForId(genreId);
            if (genre != null) {
                context.json(genre);
            } else {
                context.status(Constants.NOT_FOUND_404);
            }
        } catch (SQLException e) {
            logger.error("Error occurred getting records");
            context.status(Constants.INTERNAL_SERVER_ERROR_500);
        }
    }

    /**
     * Метод, отвечающий за изменение существующей записи в БД.
     * Соответствует PATCH методу в HTTP. (PATCH genres/:id)
     *
     * "@NotNull" - контракт в Java, который говорит, что если Context == null, выбросить исключение и остановить программу.
     * */
    @Override
    public void update(@NotNull Context context, @NotNull String s) {
        long genreId = Long.valueOf(s);
        Genre newGenre = context.bodyAsClass(Genre.class);
        newGenre.setId(genreId);
        try {
            dao.update(newGenre);
        } catch (SQLException e) {
            logger.error("Error occurred getting records");
            context.status(Constants.INTERNAL_SERVER_ERROR_500);
        }
    }
}
