package org.bitbucket.anuarkaliyev23.film.sample.controller;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import io.javalin.Context;
import io.javalin.apibuilder.CrudHandler;
import org.bitbucket.anuarkaliyev23.film.sample.configuration.Constants;
import org.bitbucket.anuarkaliyev23.film.sample.configuration.DatabaseUtils;
import org.bitbucket.anuarkaliyev23.film.sample.model.Film;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

/**
 * Контроллер для модели фильмов
 *
 * {@link CrudHandler} - Интерфейс джавалина, с помощью которого мы можем описать классическое CRUD-поведение.
 * CRUD расшифровывается как CREATE, READ, UPDATE, DELETE.
 * То есть CRUD - методы, служащие для создания, чтения, изменения и удаления записей из БД.
 * Мы, конечно, можем им не пользоваться, и делать все вручную (но я не стал)
 *
 * Существует конвенция соответствия HTTP реквестов CRUD методам
 * HTTP POST - CREATE
 * HTTP GET - READ
 * HTTP PATCH - UPDATE
 * HTTP DELETE - DELETE
 *
 * То есть создаются пути
 *(САМИ ПУТИ ОПИСЫВАЮТСЯ В {@link org.bitbucket.anuarkaliyev23.film.sample.Main}! ЗДЕСЬ ПРОСТО COPY-PASTE)
 * GET /films
 * GET /films/:id
 * POST /films
 * PATCH /films/:id
 * DELETE /films/:id
 *
 * @see org.bitbucket.anuarkaliyev23.film.sample.Main - там лежат пути javalin
 * Пожалкйста, обратитесь к документации javalin, если хотите узнать дополнительно про CrudHandler.
 * */
public class FilmController implements CrudHandler {

    /**
     * DAO - паттерн разработки, при котором вся логика доступа к БД прячется в Data Access Object - DAO.
     * То есть любые операции, требующие изменения или считывания с БД должны идти через объект-посредник DAO.
     *
     * У ORMLite есть своя реализация этого паттерна, выраженная в классе Dao.
     * Dao<Film, Long> - Мы указываем, что это ДАО к сущности Фильм, а вторым параметром мы указываем ПЕРВИЧНЫЙ КЛЮЧ. То есть Long - в нашем случае
     * (Потому, что {@link Film#id - long})
     *
     * @see Dao
     * */
    private Dao<Film, Long> dao;

    /**
     * Логгер, чтобы мы могли писать в консольку и лучше понимать, что происходит, и если что-то пойдет не так,
     * понять это быстрее.
     * */
    private Logger logger;

    public FilmController() {
        //При создании объекта мы сразу создаем ему логгер
        logger = LoggerFactory.getLogger(this.getClass());
        try {
            //И инициализируем ДАО
            dao = DaoManager.createDao(DatabaseUtils.getSource(), Film.class);
        } catch (SQLException e) {
            logger.error("Error creating DAO");
        }
    }

    /**
     * Метод, отвечающий за создание новой записи в БД.
     * Соответствует POST методу в HTTP. (POST films/)
     *
     * "@NotNull" - контракт в Java, который говорит, что если Context == null, выбросить исключение и остановить программу.
     * */
    @Override
    public void create(@NotNull Context context) {
        //Десериализуем JSON из тела запроса в Film
        Film film = context.bodyAsClass(Film.class);
        try {
            //Создаем запись в БД
            dao.create(film);
            //Отдаем 201 статус в ответ
            context.status(Constants.CREATED_201);
        } catch (SQLException e) {
            logger.error("Error occurred saving record");
            //При ошибке - отдаем 500 статус
            context.status(Constants.INTERNAL_SERVER_ERROR_500);
        }
    }

    /**
     * Метод, отвечающий за удаление записи в БД.
     * Соответствует DELETE методу в HTTP. (DELETE films/:id)
     *
     * "@NotNull" - контракт в Java, который говорит, что если Context == null, выбросить исключение и остановить программу.
     * */
    @Override
    public void delete(@NotNull Context context, @NotNull String s) {
        //Считываем ID записи, которую нужно удалить
        long filmId = Long.valueOf(s);
        try {
            //Удаляем
            dao.deleteById(filmId);
        } catch (SQLException e) {
            logger.error("Error occurred deleting records");
            context.status(Constants.INTERNAL_SERVER_ERROR_500);
        }
    }

    /**
     * Метод, отвечающий за считывание ВСЕХ записей с БД.
     * Соответствует GET методу в HTTP. (GET films/)
     *
     * "@NotNull" - контракт в Java, который говорит, что если Context == null, выбросить исключение и остановить программу.
     * */
    @Override
    public void getAll(@NotNull Context context) {
        try {
            //Достаем все записи и сериализуем их в JSON
            context.json(dao.queryForAll());
        } catch (SQLException e) {
            logger.error("Error occurred listing records");
            context.status(Constants.INTERNAL_SERVER_ERROR_500);
        }
    }

    /**
     * Метод, отвечающий за считывание ОДНОЙ записи с БД.
     * Соответствует GET методу в HTTP. (GET films/:id)
     *
     * "@NotNull" - контракт в Java, который говорит, что если Context == null, выбросить исключение и остановить программу.
     * */
    @Override
    public void getOne(@NotNull Context context, @NotNull String s) {
        //Читаем ID записи, которую хочет считать клиент
        long filmId = Long.valueOf(s);
        try {
            //Ищем в БД
            Film film = dao.queryForId(filmId);
            if (film != null) {
                //Если такая запись есть, сериализуем и отдаем
                context.json(film);
            } else {
                //Если нет - отдаем 404 статус
                context.status(Constants.NOT_FOUND_404);
            }
        } catch (SQLException e) {
            logger.error("Error occurred getting records");
            context.status(Constants.INTERNAL_SERVER_ERROR_500);
        }
    }


    /**
     * Метод, отвечающий за изменение существующей записи в БД.
     * Соответствует PATCH методу в HTTP. (PATCH films/:id)
     *
     * "@NotNull" - контракт в Java, который говорит, что если Context == null, выбросить исключение и остановить программу.
     * */
    @Override
    public void update(@NotNull Context context, @NotNull String s) {
        //Читаем ID записи, которую хотим изменить
        long filmId = Long.valueOf(s);
        //Считываем JSON из тела запроса
        Film newFilm = context.bodyAsClass(Film.class);
        //Меняем ID на ID существующей записи
        newFilm.setId(filmId);
        try {
            //Меняем запись в БД
            dao.update(newFilm);
        } catch (SQLException e) {
            logger.error("Error occurred getting records");
            context.status(Constants.INTERNAL_SERVER_ERROR_500);
        }
    }
}
