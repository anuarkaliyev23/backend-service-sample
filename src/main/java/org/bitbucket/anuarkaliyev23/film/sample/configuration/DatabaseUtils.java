package org.bitbucket.anuarkaliyev23.film.sample.configuration;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.bitbucket.anuarkaliyev23.film.sample.exceptions.InvalidConnectionSourceException;
import org.bitbucket.anuarkaliyev23.film.sample.model.*;

import java.sql.SQLException;

/**
* Класс, в котором лежат утилиты для БД
* */
public class DatabaseUtils {
    /**
     * Соединение с БД.
     * Мы должны указать где ORMlite искать нашу бд.
     * Так как у нас одна база и одно соединение (к SQLite БД не имеет смысла иметь много соединений)
     * мы просто храним одно статическое соединение на весь проект.
     * */
    private static ConnectionSource source;

    /*Статический блок.

      Статические блоки и статическая инициализация используются для инициализации статических полей в Java. Мы объявляем статический блок в тот момент, когда хотим проинициализировать статические поля в классе.

      Статический блок выполняется только один раз, при загрузке класса загрузчиком.

      То есть то, что вы пишите в статическом блоке выполняется один раз за весь проект при загрузке класса (В нашем случае - с самого запуска)
      */
    static {
        try {
            //Соединяемся с БД. Если такой БД нет, SQLite драйвер создась бд сам. <- Применимо ТОЛЬКО к SQLite
            source = new JdbcConnectionSource(Constants.DB_PATH);

            /*Создаем таблицы по образцу наших классов, если они еще не созданы (Например, если база только создалась)*/
            TableUtils.createTableIfNotExists(source, Film.class);
            TableUtils.createTableIfNotExists(source, Job.class);
            TableUtils.createTableIfNotExists(source, Person.class);
            TableUtils.createTableIfNotExists(source, Genre.class);
            TableUtils.createTableIfNotExists(source, FilmGenre.class);
            TableUtils.createTableIfNotExists(source, FilmPerson.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Функция, возвращающая Соединение с БД
     *
     * @throws InvalidConnectionSourceException - Кидает исключение, если не законнектилось или не проинициализоровалось соединение
     * @return Соединение с базой
     * */
    public static ConnectionSource getSource() {
        if (source == null) throw new InvalidConnectionSourceException();
        return source;
    }

    private DatabaseUtils(){}
}
