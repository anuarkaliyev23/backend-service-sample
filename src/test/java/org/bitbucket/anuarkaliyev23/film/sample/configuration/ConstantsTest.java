package org.bitbucket.anuarkaliyev23.film.sample.configuration;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConstantsTest {

    @Test
    void testPort() {
        assertEquals(Constants.PORT, 7645);
    }

    @Test
    void testDBPath() {
        assertEquals(Constants.DB_PATH, "jdbc:sqlite:film.db");
    }
}
